FROM endesapdi/generic:latest

MAINTAINER PDI

ENV JAVA_PACKAGE java-11-openjdk-devel

RUN zypper up -y \
    && zypper -q --non-interactive install \
              xmlstarlet saxon9 augeas \
              ${JAVA_PACKAGE} \
    && zypper clean -a

ENV JAVA_HOME /usr/lib64/jvm/jre
